<!doctype html>
<html class="no-js" lang="zxx">

<head>
	<base href="<?php echo $site; ?>" />
   <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $depKeyword; ?> - êtes-vous éligible aux aides panneaux solaires</title>
    <meta name="description" content="<?php echo $depKeyword; ?>. Profitez des aides panneaux solaires qui se terminent en 2020" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div class="header-top_area d-none d-lg-block">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-4 col-lg-4">
                            <div class="logo">
                                <a href="<?php echo $_SERVER['SCRIPT_URL']; ?>">
                                    <img src="img/logo2.png" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-md-8">
                            <div class="header_right d-flex align-items-center">
                                <div class="short_contact_list">
                                	<?php
									/*
                                    <ul>
                                        <li><a href="#"> <i class="fa fa-envelope"></i> info@docmed.com</a></li>
                                        <li><a href="#"> <i class="fa fa-phone"></i> 1601-609 6780</a></li>
                                    </ul>
									*/
									?>
                                </div>

                                <div class="book_btn d-none d-lg-block">
                                    <a class="boxed-btn3-line" href="#simulateur" style="color:#090;border-color:#090;">Vérifier mon éligibilité</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
			/*
            <div id="sticky-header" class="main-header-area">
                <div class="container">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-12 d-block d-lg-none">
                                <div class="logo">
                                    <a href="index.html">
                                        <img src="img/logo.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-9">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a  href="index.html">home</a></li>
                                            <li><a  href="service.html">Services</a></li>
                                            <li><a href="About.html">about</a></li>
                                            <li><a href="#">pages <i class="ti-angle-down"></i></a>
                                                <ul class="submenu">
                                                    <li><a href="service_details.html">service details</a></li>
                                                    <li><a href="elements.html">elements</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">blog <i class="ti-angle-down"></i></a>
                                                <ul class="submenu">
                                                    <li><a href="blog.html">blog</a></li>
                                                    <li><a href="single-blog.html">single-blog</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.html">Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                                <div class="Appointment justify-content-end">
                                    <div class="search_btn">
                                        <a data-toggle="modal" data-target="#exampleModalCenter" href="#">
                                            <i class="ti-search"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
			*/
			?>
        </div>


<?php
/*
PROMO PNEU

<!-- Global site tag (gtag.js) - Google Ads: 665378237 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-665378237"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-665378237');
</script>
*/
?>
<!-- Global site tag (gtag.js) - Google Ads: 834058162 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-834058162"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-834058162');
</script>

<?php
if($leadOK) { ?>
<?php
/*
PROMO PNEU 
<!-- Event snippet for Contact conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-665378237/5sP3CNr5otEBEL27o70C'});
</script>
*/

?>
<!-- Event snippet for Demande de devis conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-834058162/HBskCMKni9ABELLv2o0D'});
</script>

<?php
} ?>


    </header>
    <!-- header-end -->
    <?php
if(isset($leadOK)) { ?>
<section style="background-color:#5cb85c;padding:0;color:#ffffff;padding-top:5px;padding-bottom:5px;position:fixed;top:0;width:100%;z-index:99999;">
	<div class="container">
    Vous êtes éligible, mais nous avons besoin de plus de détail pour pouvoir calculer votre aide, notre équipe vous recontactera au plus vite du lundi au vendredi entre 9h et 18h.
	</div>
</section><?php


} ?>
    <!-- slider_area_start -->
    <div class="slider_area">
        <div class="single_slider  d-flex align-items-center" style="background-image: url(img/banner/banner2.png);">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-8">
                        <div class="slider_text text-center justify-content-center">
                            <p>Prime énergie verte 2020</p>
                            <h3>Jusqu'à 10500€ financé par l'état</h3>
                                <a class="boxed-btn3" href="#simulateur" style="background-color:#090;">Vérifier mon éligibilité</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <div class="transportaion_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_transport">
                    <?php
					/*
                        <div class="icon">
                            <img src="img/svg_icon/world.png" alt="">
                        </div>
						*/
						?>
                        <h3>Aides 2020</h3>
                        <p>Profitez des aides de l'état qui ne sont valables que jusqu'à fin 2020. Ne ratez pas l'occasion.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_transport">
                    <?php
					/*
                        <div class="icon">
                            <img src="img/svg_icon/live.png" alt="">
                        </div>
					*/
					?>
                        <h3>Formulaire en direct</h3>
                        <p>Remplissez le formulaire d'éligibilité et sachez instantanément si vous êtes éligible à l'aide de l'état.</p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_transport">
                    <?php
					/*
                        <div class="icon">
                            <img src="img/svg_icon/world.png" alt="">
                        </div>
					*/
					?>
                        <h3>Un geste pour la planète</h3>
                        <p>Protégez la planète en chauffant votre maison avec des énergies vertes, et réalisez des économies.</p>
                    </div>
                </div>
            </div>
            <div class="row" style="padding-top:50px;">

                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_transport">
                        <img src="img/logo/edf-prime-energie.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_transport">
                        <img src="img/logo/certificatr-economie-energie-pac.jpg" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_transport">
                        <img src="img/logo/anah-pompe-a-chaleur.jpg" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-counter bg-light ftco-no-pt" id="simulateur">
    	<div class="container">
				<div class="row">
                	<div class="col-md-6 offset-md-3">

<!-- Modal -->
<br /><br />
      	<h2 style="color:#390;font-weight:bold;width:100%;text-align:center;">Simulateur d'éligibilité</h2>

      	<form method="post" action="<?php echo $_SERVER['SCRIPT_URL']; ?>">
        <input type="hidden" name="submit_lead" value="1" />
        <input type="hidden" name="type_lead" value="panneau" />
      <div class="modal-body">
		<div><span style="color:#F00;">Les données que vous allez renseigner dans le formulaire ne seront jamais revendues à des sociétés commerciales ou utilisées dans des buts autres que pour l'égibilité de l'aide de la prime energie verte.<br /><br /></span></div>
      	<div class="form-group">
		  <?php /* <label for="inputState55">Budget</label> */ ?>
          <select name="lead[vous_habitez]" class="form-control" required>
                <option value="" disabled selected hidden>Vous habitez</option>
                <option value="maison">Une maison</option>
                <option value="appart">Un appartement</option>
          </select>
		</div>
      	<div class="form-group">
		  <?php /* <label for="inputState55">Budget</label> */ ?>
          <select name="lead[vous_etes]" class="form-control" required>
                <option value="" disabled selected hidden>Vous êtes</option>
                <option value="proprio">Propriétaire</option>
                <option value="locataire">Locataire</option>
          </select>
		</div>
      	<div class="form-group">
		  <?php /* <label for="inputState55">Budget</label> */ ?>
          <select name="lead[type_chauffage]" class="form-control" required>
                <option value="" disabled selected hidden>Type de chauffage</option>
                <option value="gaz">Gaz</option>
                <option value="electrique">Electrique</option>
                <option value="fioul">Fioul</option>
                <option value="bois">Bois / Granulé</option>
                <option value="autre">Autre</option>
                
          </select>
		</div>
      	<div class="form-group">
		  <?php /* <label for="inputState55">Budget</label> */ ?>
          <select name="lead[montant_facture]" class="form-control" required>
                <option value="" disabled selected hidden>Montant annuel de votre facture de chauffage</option>
                <option value="-1400">Moins de 1400€</option>
                <option value="1400-1600">De 1400 à 1600€</option>
                <option value="1600-1800">De 1600€ à 1800€</option>
                <option value="1800-2000">De 1800€ à 2000€</option>
                <option value="2000+">Plus de 2000€</option>
          </select>
		</div>
      	<div class="form-group">
			<?php /* <label for="inputName">Nom & Prénom</label> */ ?>
			<input type="text" class="form-control" name="lead[nom]" placeholder="Nom & Prénom" required />        	
        </div>
      	<div class="form-group">
			<?php /* <label for="inputName">Nom & Prénom</label> */ ?>
			<input type="number" class="form-control" name="lead[telephone]" placeholder="Numéro de téléphone" required />        	
        </div>
      	<div class="form-group">
			<?php /* <label for="inputName">Nom & Prénom</label> */ ?>
			<input type="email" class="form-control" name="lead[email]" placeholder="Email" required />        	
        </div>
      	<div class="form-group">
			<?php /* <label for="inputName">Nom & Prénom</label> */ ?>
			<input type="text" class="form-control" name="lead[code_postal]" placeholder="Code postal" required />  
               <?php
			/*
         
<select class="form-control form-control-md" name="lead[code_postal]" required>
    <option value="">Département</option>
    <option value="01">01 - Ain </option>
    <option value="02">02 - Aisne </option>
    <option value="03">03 - Allier </option>
    <option value="04">04 - Alpes de Haute Provence </option>
    <option value="05">05 - Hautes Alpes </option>
    <option value="06">06 - Alpes Maritimes </option>
    <option value="07">07 - Ardèche </option>
    <option value="08">08 - Ardennes </option>
    <option value="09">09 - Ariège </option>
    <option value="10">10 - Aube </option>
    <option value="11">11 - Aude </option>
    <option value="12">12 - Aveyron </option>
    <option value="13">13 - Bouches du Rhône </option>
    <option value="14">14 - Calvados </option>
    <option value="15">15 - Cantal </option>
    <option value="16">16 - Charente </option>
    <option value="17">17 - Charente Maritime </option>
    <option value="18">18 - Cher </option>
    <option value="19">19 - Corrèze </option>
    <option value="2A">2A - Corse du Sud </option>
    <option value="2B">2B - Haute-Corse </option>
    <option value="21">21 - Côte d'Or </option>
    <option value="22">22 - Côtes d'Armor </option>
    <option value="23">23 - Creuse </option>
    <option value="24">24 - Dordogne </option>
    <option value="25">25 - Doubs </option
    ><option value="26">26 - Drôme </option>
    <option value="27">27 - Eure </option>
    <option value="28">28 - Eure et Loir </option>
    <option value="29">29 - Finistère </option>
    <option value="30">30 - Gard </option>
    <option value="31">31 - Haute Garonne </option>
    <option value="32">32 - Gers </option>
    <option value="33">33 - Gironde </option>
    <option value="34">34 - Hérault </option>
    <option value="35">35 - Ille et Vilaine </option>
    <option value="36">36 - Indre </option>
    <option value="37">37 - Indre et Loire </option>
    <option value="38">38 - Isère </option>
    <option value="39">39 - Jura </option>
    <option value="40">40 - Landes </option>
    <option value="41">41 - Loir et Cher </option>
    <option value="42">42 - Loire </option>
    <option value="43">43 - Haute Loire </option>
    <option value="44">44 - Loire Atlantique </option>
    <option value="45">45 - Loiret </option>
    <option value="46">46 - Lot </option>
    <option value="47">47 - Lot et Garonne </option>
    <option value="48">48 - Lozère </option>
    <option value="49">49 - Maine et Loire </option>
    <option value="50">50 - Manche </option>
    <option value="51">51 - Marne </option>
    <option value="52">52 - Haute Marne </option>
    <option value="53">53 - Mayenne </option>
    <option value="54">54 - Meurthe et Moselle </option>
    <option value="55">55 - Meuse </option>
    <option value="56">56 - Morbihan </option>
    <option value="57">57 - Moselle </option>
    <option value="58">58 - Nièvre </option>
    <option value="59">59 - Nord </option>
    <option value="60">60 - Oise </option>
    <option value="61">61 - Orne </option>
    <option value="62">62 - Pas de Calais </option>
    <option value="63">63 - Puy de Dôme </option>
    <option value="64">64 - Pyrénées Atlantiques </option>
    <option value="65">65 - Hautes Pyrénées </option>
    <option value="66">66 - Pyrénées Orientales </option>
    <option value="67">67 - Bas Rhin </option>
    <option value="68">68 - Haut Rhin </option>
    <option value="69">69 - Rhône </option>
    <option value="70">70 - Haute Saône </option>
    <option value="71">71 - Saône et Loire </option>
    <option value="72">72 - Sarthe </option>
    <option value="73">73 - Savoie </option>
    <option value="74">74 - Haute Savoie </option>
    <option value="75">75 - Paris </option>
    <option value="76">76 - Seine Maritime </option>
    <option value="77">77 - Seine et Marne </option>
    <option value="78">78 - Yvelines </option>
    <option value="79">79 - Deux Sèvres </option>
    <option value="80">80 - Somme </option>
    <option value="81">81 - Tarn </option>
    <option value="82">82 - Tarn et Garonne </option>
    <option value="83">83 - Var </option>
    <option value="84">84 - Vaucluse </option>
    <option value="85">85 - Vendée </option>
    <option value="86">86 - Vienne </option>
    <option value="87">87 - Haute Vienne </option>
    <option value="88">88 - Vosges </option>
    <option value="89">89 - Yonne </option>
    <option value="90">90 - Territoire de Belfort </option>
    <option value="91">91 - Essonne </option>
    <option value="92">92 - Hauts de Seine </option>
    <option value="93">93 - Seine Saint Denis </option>
    <option value="94">94 - Val de Marne </option>
    <option value="95">95 - Val d'Oise </option>
</select>   	
			*/
			?> 
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" style="width:100%;">VERIFIER MON ELIBILITE</button>
      </div>
      </form>
      <br /><br />
</div>
</div>
</div>
<section>


    <!-- contact_action_area  -->
    <div class="contact_action_area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-7 col-md-6">
                    <div class="action_heading">
                        <h3>100% rapide et sécurisé</h3>
                        <p>Découvrez en quelques minutes combien vous pourrez percevoir d'aides de l'état afin de financer votre installation photovoltaique</p>
                    </div>
                </div>
                <div class="col-xl-5 col-md-6">
                    <div class="call_add_action">
                        <a href="#simulateur" class="boxed-btn3" style="background-color:#090;">Tester mon éligibilité</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- counter_area  -->
    <div class="counter_area">
        <div class="container">
            <div class="offcan_bg">
                <div class="row">
                    <div class="col-xl-3 col-md-3">
                        <div class="single_counter text-center">
                            <h3> <span class="counter">500</span> <span>+</span> </h3>
                            <p>Panneaux solaires installées</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3">
                        <div class="single_counter text-center">
                            <h3> <span class="counter">4M</span> <span>+</span> </h3>
                            <p>d'aides de l'état</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3">
                        <div class="single_counter text-center">
                            <h3> <span class="counter">500</span></h3>
                            <p>clients heureux</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-3">
                        <div class="single_counter text-center">
                            <h3> <span class="counter">6</span></h3>
                            <p>agences en France</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- /counter_area  -->



    <!-- contact_location  -->
    <div class="contact_location">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="location_left">
                        <div class="logo">
                            <a href="simulateursimulateur">
                                <img src="img/logo2.png" alt="">
                            </a>
                        </div>
                        <ul>
                            <li><a href="#simulateur"> <i class="fa fa-facebook"></i> </a></li>
                            <li><a href="#simulateur"> <i class="fa fa-google-plus"></i> </a></li>
                            <li><a href="#simulateur"> <i class="fa fa-twitter"></i> </a></li>
                            <li><a href="#simulateur"> <i class="fa fa-youtube"></i> </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-md-3">
                    <div class="single_location">
                        <h3> <img src="img/icon/address.svg" alt="">Panneaux solaires</h3>
                        <p>Aides programme PAC<br />
                            Prime énergie verte</p>
                    </div>
                </div>
                <div class="col-xl-3 col-md-3">
                    <div class="single_location">
                        <h3> <img src="img/icon/support.svg" alt=""> Service client</h3>
                        <p>Disponible du lundi au vendredi<br />de 8h à 18h</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--/ contact_location  -->

    <!-- footer start -->
    <footer class="footer">
<?php
/*
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Services
                            </h3>
                            <ul>
                                <li><a href="#">Air Transportation</a></li>
                                <li><a href="#">Ocean Freight</a></li>
                                <li><a href="#">Ocean Cargo</a></li>
                                <li><a href="#">Logistics</a></li>
                                <li><a href="#">Warehouse Moving</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Company
                            </h3>
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">News</a></li>
                                <li><a href="#"> Testimonials</a></li>
                                <li><a href="#"> Why Us?</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Industries
                            </h3>
                            <ul>
                                <li><a href="#">Chemicals</a></li>
                                <li><a href="#">Automotive</a></li>
                                <li><a href="#"> Consumer Goods</a></li>
                                <li><a href="#">Life Science</a></li>
                                <li><a href="#">Foreign Trade</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Subscribe
                            </h3>
                            <form action="#" class="newsletter_form">
                                <input type="text" placeholder="Enter your mail">
                                <button type="submit">Subscribe</button>
                            </form>
                            <p class="newsletter_text">Esteem spirit temper too say adieus who direct esteem esteems
                                luckily.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		*/
 ?> 
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Prime énergie verte est une marqué déposée <i class="fa fa-heart-o" aria-hidden="true"></i>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->
<!-- Button trigger modal -->
 
  <!-- Modal -->

    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <!-- <script src="js/gijgo.min.js"></script> -->
    <script src="js/slick.min.js"></script>



    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>


    <script src="js/main.js"></script>




</body>

</html>