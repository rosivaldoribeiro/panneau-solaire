<!doctype html>
<html class="no-js" lang="zxx">

<head>
	<base href="<?php echo $site; ?>" />
   <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $depKeyword; ?> - êtes-vous éligible aux aides pompe à chaleur pac</title>
    <meta name="description" content="<?php echo $depKeyword; ?>. Profitez des aides pompe à chaleur pac qui se terminent en 2020" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/gijgo.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slicknav.css">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->
<!-- Global site tag (gtag.js) - Google Ads: 834058162 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-834058162"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-834058162');
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '2778858472178775');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=288302152364822&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Event snippet for Demande de devis conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-834058162/HBskCMKni9ABELLv2o0D'});
</script>
<?php
/*
<script>
  fbq('track', 'Contact');
</script>
*/
?>
<script>

fbq('track', 'Purchase', {
value: 15,
currency: 'EUR'
});
</script>
<?php
 ?>

</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div class="header-top_area d-none d-lg-block">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-4 col-lg-4">
                            <div class="logo">
                                <a href="<?php echo $_SERVER['SCRIPT_URL']; ?>">
                                   Logo Client
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-md-8">
                            <div class="header_right d-flex align-items-center">
                                <div class="short_contact_list">
                                	<?php
									/*
                                    <ul>
                                        <li><a href="#"> <i class="fa fa-envelope"></i> info@docmed.com</a></li>
                                        <li><a href="#"> <i class="fa fa-phone"></i> 1601-609 6780</a></li>
                                    </ul>
									*/
									?>
                                </div>

                                <div class="book_btn d-none d-lg-block">
                                    <a class="boxed-btn3-line" href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateurform" style="color:#090;border-color:#090;">Estimer mes économies</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
			/*
            <div id="sticky-header" class="main-header-area">
                <div class="container">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-12 d-block d-lg-none">
                                <div class="logo">
                                    <a href="index.html">
                                        <img src="img/logo.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-9">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a  href="index.html">home</a></li>
                                            <li><a  href="service.html">Services</a></li>
                                            <li><a href="About.html">about</a></li>
                                            <li><a href="#">pages <i class="ti-angle-down"></i></a>
                                                <ul class="submenu">
                                                    <li><a href="service_details.html">service details</a></li>
                                                    <li><a href="elements.html">elements</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">blog <i class="ti-angle-down"></i></a>
                                                <ul class="submenu">
                                                    <li><a href="blog.html">blog</a></li>
                                                    <li><a href="single-blog.html">single-blog</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.html">Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                                <div class="Appointment justify-content-end">
                                    <div class="search_btn">
                                        <a data-toggle="modal" data-target="#exampleModalCenter" href="#">
                                            <i class="ti-search"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
			*/
			?>
        </div>


<?php
/*
PROMO PNEU

<!-- Global site tag (gtag.js) - Google Ads: 665378237 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-665378237"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-665378237');
</script>
*/
?>





    </header>
    <!-- header-end -->
    <?php
if(isset($leadISO)) { ?>
<section style="background-color:#5cb85c;padding:0;color:#ffffff;padding-top:5px;padding-bottom:5px;position:fixed;top:0;width:100%;z-index:99999;">
	<div class="container">
    Vous êtes éligible, mais nous avons besoin de plus de détail pour pouvoir calculer votre aide, notre équipe vous recontactera au plus vite du lundi au vendredi entre 9h et 18h.
	</div>
</section><?php	
}
if(isset($leadOK) && !isset($leadISO)) { ?>
<div style="height:100%;">
<section style="background-color:#5cb85c;padding:0;color:#ffffff;padding-top:15px;padding-bottom:20px;width:100%;z-index:99999;">
	<div class="container">
    Vous êtes éligible, mais notre système n'a pas réussi à définir exactement le montant auquel vous êtes éligible en raison d'un critère qui nécessite de vous contacter. Un conseiller vous contactera alors par téléphone ou email dans les prochaines 24h. En attendant, nous avons une information importante à vous communiquer,<br />lisez ci-dessous!
	</div>
	<div style="background-color:#36C;color:#fff;padding:30px;">
        <div class="container">
            <h1 style="font-ewight:bold;color:#fff;">Le saviez-vous?</h1>
            <p style="color:#fff;">
            L'état a mis de nombreuses aides autres que la pompe à chaleur.<br />
            
            Une autre aide de l'état, tous les français peuvent en bénéficier et elle vous permet d'isoler votre maison pour 1€ symbolique.<br />
            Avec cette aide vous pouvez isoler:
            <ul>
                <li>Les combles de votre maison, Le garage, Votre cave, Le plancher</li>
            </ul>
            <br />
            Vous êtes intéressé(e)? C'est très simple, vous avez juste à nous dire quelle partie de la maison vous souhaitez isoler et cliquer sur le bouton valider.<br /><br />
            
            <form method="post" action="<?php echo $_SERVER['SCRIPT_URL']; ?>">
            <input type="hidden" name="submit_lead" value="1" />
            <input type="hidden" name="type_lead" value="iso_1_euro" />
      	<div class="form-group">
		  <?php /* <label for="inputState55">Budget</label> */ ?>
          <select name="lead[type_projet]" class="form-control" required>
                <option value="combles_plancher">Combles + Plancher</option>
                <option value="combles">Combles perdus</option>
                <option value="plancher">Plancher (Sous-sol, Cave, Vide-sanitaires)</option>
                <option value="garage">Garage (Attenant, Plafond)</option>
          </select>
		</div>

            <div class="form-group" style="display:none;">
              <?php /* <label for="inputState55">Budget</label> */ ?>
              <select name="lead[vous_habitez]" class="form-control" required>
                    <option value="" disabled selected hidden>Vous habitez</option>
                    <option value="maison" <?php if($_POST['lead']['vous_habitez'] == 'maison') { echo 'selected'; } ?>>Une maison</option>
                    <option value="appart" <?php if($_POST['lead']['vous_habitez'] == 'appart') { echo 'selected'; } ?>>Un appartement</option>
              </select>
            </div>
            <div class="form-group" style="display:none;">
              <?php /* <label for="inputState55">Budget</label> */ ?>
              <select name="lead[vous_etes]" class="form-control" required>
                    <option value="" disabled selected hidden>Vous êtes</option>
                    <option value="proprio" <?php if($_POST['lead']['vous_etes'] == 'proprio') { echo 'selected'; } ?>>Propriétaire</option>
                    <option value="locataire" <?php if($_POST['lead']['vous_etes'] == 'locataire') { echo 'selected'; } ?>>Locataire</option>
              </select>
            </div>
            <div class="form-group" style="display:none;">
              <?php /* <label for="inputState55">Budget</label> */ ?>
              <select name="lead[type_chauffage]" class="form-control" required>
                    <option value="" disabled selected hidden>Type de chauffage</option>
                    <option value="gaz" <?php if($_POST['lead']['type_chauffage'] == 'gaz') { echo 'selected'; } ?>>Gaz</option>
                    <option value="electrique" <?php if($_POST['lead']['type_chauffage'] == 'electrique') { echo 'selected'; } ?>>Electrique</option>
                    <option value="fioul" <?php if($_POST['lead']['type_chauffage'] == 'fioul') { echo 'selected'; } ?>>Fioul</option>
                    <option value="bois" <?php if($_POST['lead']['type_chauffage'] == 'bois') { echo 'selected'; } ?>>Bois / Granulé</option>
                    <option value="autre" <?php if($_POST['lead']['type_chauffage'] == 'autre') { echo 'selected'; } ?>>Autre</option>
                    
              </select>
            </div>
            <div class="form-group" style="display:none;">
              <?php /* <label for="inputState55">Budget</label> */ ?>
              <select name="lead[montant_facture]" class="form-control" required>
                    <option value="" disabled selected hidden>Montant annuel de votre facture de chauffage</option>
                    <option value="-1400" <?php if($_POST['lead']['montant_facture'] == '-1400') { echo 'selected'; } ?>>Moins de 1400€</option>
                    <option value="1400-1600" <?php if($_POST['lead']['montant_facture'] == '1400-1600') { echo 'selected'; } ?>>De 1400 à 1600€</option>
                    <option value="1600-1800" <?php if($_POST['lead']['montant_facture'] == '1600-1800') { echo 'selected'; } ?>>De 1600€ à 1800€</option>
                    <option value="1800-2000" <?php if($_POST['lead']['montant_facture'] == '1800-2000') { echo 'selected'; } ?>>De 1800€ à 2000€</option>
                    <option value="2000+" <?php if($_POST['lead']['montant_facture'] == '2000+') { echo 'selected'; } ?>>Plus de 2000€</option>
              </select>
            </div>
            <div class="form-group" style="display:none;">
                <?php /* <label for="inputName">Nom & Prénom</label> */ ?>
                <input type="text" class="form-control" name="lead[nom]" placeholder="Nom & Prénom" required value="<?php echo $_POST['lead']['nom']; ?>" />        	
            </div>
            <div class="form-group" style="display:none;">
                <?php /* <label for="inputName">Nom & Prénom</label> */ ?>
                <input type="number" class="form-control" name="lead[telephone]" placeholder="Numéro de téléphone" required value="<?php echo $_POST['lead']['telephone']; ?>" />        	
            </div>
            <div class="form-group" style="display:none;">
                <?php /* <label for="inputName">Nom & Prénom</label> */ ?>
                <input type="email" class="form-control" name="lead[email]" placeholder="Email" required value="<?php echo $_POST['lead']['email']; ?>" />        	
            </div>
            <div class="form-group" style="display:none;">
                <?php /* <label for="inputName">Nom & Prénom</label> */ ?>
                <input type="text" class="form-control" name="lead[code_postal]" placeholder="Code postal" required value="<?php echo $_POST['lead']['code_postal']; ?>" />  
            </div>
            <div class="form-group">
            <button type="submit" class="btn btn-success" style="width:100%;">Je veux aussi l'isolation à 1€ de ma maison</button>
          </div>
          </form>
            </p>
        </div>
     </div>
</section>
</div><?php


}
else { ?>
    <!-- slider_area_start -->
    <div class="slider_area">
        <div class="single_slider  d-flex align-items-center" style="background-image: url(img/banner/banner3.jpg);">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-8">
                        <div class="slider_text text-center justify-content-center">
                            <p>sub text here</p>
                            <h3>Vous voulez en finir avec les factures d'électricité</h3>
                                <a class="boxed-btn3" href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateurform" style="background-color:#090;">Estimer mes économies</a>
</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <div class="transportaion_area">
        <div class="container">
        <div class="row d-flex align-items-center">
					<div class="col-md-3">
						<div class="trait bg-success w-100">
							&nbsp;
						</div>
					</div>

					<div class="col-md-6">
						<h2><center>Comment ça marche ?</center></h2>
                        </br>
					</div>

					<div class="col-md-3">
						<div class="trait bg-success w-100">
							&nbsp;
						</div>
					</div>
				</div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="single_transport">
                    <center>
                    <div class="icon">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="100" height="100"viewBox="0 0 172 172" style=" fill:#000000;">
                    <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#009900"><path d="M112.875,21.5c-2.28437,0 -4.03125,1.74687 -4.03125,4.03125v6.71875c0,2.28437 1.74688,4.03125 4.03125,4.03125c2.28437,0 4.03125,-1.74688 4.03125,-4.03125v-6.71875c0,-2.28438 -1.74688,-4.03125 -4.03125,-4.03125zM142.30103,29.33154c-1.30176,0.09973 -2.55103,0.8031 -3.2229,1.97888l-3.35937,5.9104c-1.075,1.88125 -0.40365,4.43647 1.4776,5.51147c0.67188,0.40312 1.34375,0.5354 2.01563,0.5354c1.34375,0 2.82135,-0.67187 3.49323,-2.01562l3.35938,-5.9104c1.20937,-1.88125 0.53802,-4.43647 -1.4776,-5.51147c-0.70547,-0.40313 -1.50489,-0.5585 -2.28595,-0.49866zM82.37292,30.03491c-0.78105,-0.04015 -1.58048,0.15012 -2.28595,0.60364c-1.88125,1.075 -2.5526,3.6276 -1.4776,5.50885l3.35938,5.6427c0.80625,1.20938 2.14947,2.01563 3.49322,2.01563c0.67188,0 1.34375,-0.1349 2.01563,-0.53802c1.88125,-1.075 2.5526,-3.6276 1.4776,-5.50885l-3.35937,-5.77655c-0.67187,-1.17578 -1.92114,-1.88046 -3.2229,-1.94739zM112.875,48.50885c-15.18438,0 -28.89115,9.13907 -34.80365,23.11407c-0.80625,2.01563 0.13385,4.43228 2.14948,5.23853c2.01563,0.80625 4.4349,-0.13385 5.24115,-2.14948c4.70313,-11.15313 15.45365,-18.27447 27.41303,-18.27447c-2.28437,0 -4.03125,1.74687 -4.03125,4.03125c0,2.28438 1.74688,4.03125 4.03125,4.03125c11.825,0 21.5,9.675 21.5,21.5c0,2.28437 1.74687,4.03125 4.03125,4.03125c2.28438,0 4.03125,-1.74688 4.03125,-4.03125c0,2.01563 -0.26927,4.03178 -0.53802,5.91303c-0.5375,2.28437 0.94167,4.29947 3.09167,4.83697c0.26875,0 0.53698,0.13385 0.80573,0.13385c1.88125,0 3.49427,-1.34427 3.8974,-3.22552c0.5375,-2.41875 0.80573,-4.97135 0.80573,-7.52448c0,-20.69375 -16.93125,-37.625 -37.625,-37.625zM165.11328,51.13861c-0.76453,-0.04724 -1.54636,0.14225 -2.25183,0.59576l-5.77655,3.35938c-1.88125,1.075 -2.5526,3.6276 -1.4776,5.50885c0.80625,1.20938 2.14948,2.01563 3.49323,2.01563c0.67188,0 1.34375,-0.1349 2.01563,-0.53802l5.77655,-3.35937c1.88125,-1.075 2.55522,-3.6276 1.48022,-5.50885c-0.75586,-1.25977 -1.98544,-1.99463 -3.25964,-2.07336zM59.9281,52.17529c-1.30176,0.09973 -2.55102,0.8031 -3.2229,1.97888c-1.075,1.88125 -0.40103,4.43385 1.48022,5.50885l5.9104,3.35938c0.67188,0.40313 1.34375,0.53802 2.01563,0.53802c1.34375,0 2.82397,-0.67187 3.49585,-2.01562c1.075,-1.88125 0.40103,-4.43647 -1.48022,-5.51147l-5.9104,-3.35937c-0.70547,-0.40312 -1.50752,-0.5585 -2.28857,-0.49866zM14.6474,81.96875c-3.49375,0 -6.71875,1.61355 -8.73438,4.43542c-2.01562,2.82187 -2.5526,6.44895 -1.4776,9.67395l10.07812,30.36822l-10.4823,39.1026c-0.5375,2.15 0.67135,4.30157 2.82135,4.97345c0.5375,0.13438 0.80677,0.13385 1.2099,0.13385c1.74687,0 3.3599,-1.20833 3.8974,-2.9552l10.75,-40.3125v-0.40417v-0.40417v-0.5354v-0.27032c0,-0.13437 -0.13385,-0.40365 -0.13385,-0.53802v-0.13385l-10.4823,-31.57812c-0.26875,-0.80625 -0.13333,-1.7453 0.40417,-2.41718c0.5375,-0.67187 1.34323,-1.07605 2.14948,-1.07605h101.99115c4.03125,0 7.65885,2.55417 8.86823,6.45105l8.60052,25.79895h-31.7146l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.22552 -5.10468,-2.55365c-2.15,0.67188 -3.22553,2.9573 -2.55365,5.1073l7.12292,21.5h-31.7146l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.22552 -5.10468,-2.55365c-2.15,0.67188 -3.22553,2.9573 -2.55365,5.1073l7.12292,21.5h-20.29272c-2.28438,0 -4.03125,1.74688 -4.03125,4.03125c0,2.28438 1.74687,4.03125 4.03125,4.03125h25.93542h0.13385h0.13385h40.17865h0.13385h0.13647h37.35468l9.5401,28.75677c0.26875,0.80625 0.13595,1.7453 -0.40155,2.41718c-0.5375,0.67188 -1.34323,1.07605 -2.14948,1.07605h-28.35522l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.22552 -5.10468,-2.55365c-2.15,0.67188 -3.22553,2.9573 -2.55365,5.1073l7.25678,21.63385h-31.84845l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.2229 -5.10468,-2.55103c-2.15,0.67188 -3.22553,2.95468 -2.55365,5.10468l7.25678,21.63385h-25.1297c-4.03125,0 -7.65885,-2.55155 -8.86823,-6.44843l-1.4776,-4.30157c-0.67187,-2.15 -2.9573,-3.2229 -5.1073,-2.55103c-2.15,0.67188 -3.2229,2.95468 -2.55103,5.10468l1.4776,4.30157c2.41875,7.12187 9.00155,11.95728 16.52655,11.95728h30.63855h40.3125h31.0401c3.49375,0 6.71875,-1.61093 8.73438,-4.4328c2.01563,-2.82188 2.5526,-6.45157 1.4776,-9.67657l-20.96198,-62.88593c-2.41875,-7.12188 -9.00155,-11.9599 -16.52655,-11.9599z"></path></g></g></svg>
                    </div>
                    </br>
                    </br>
                        <h3>Étape 1 : Vos panneaux transforment la lumière en électricité </h3>
                    </div>
                    </center>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                
                <div class="single_transport">
                    <center>
                    <div class="icon">
                    </br>
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="64" height="64" viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#009900"><path d="M70.14375,97.69063c-2.15,-0.5375 -4.3,0.80625 -4.8375,2.95625l-13.4375,53.75c-0.40312,1.88125 0.40313,3.7625 2.15,4.56875c0.5375,0.26875 1.075,0.40312 1.74687,0.40312c1.20938,0 2.41875,-0.5375 3.225,-1.6125l52.27188,-71.4875c0.94062,-1.20937 1.075,-2.82187 0.40312,-4.16562c-0.67187,-1.34375 -2.01562,-2.15 -3.62812,-2.15h-22.0375l12.49688,-62.48437c0,0 0,0 0,-0.13437c0,-0.13438 0,-0.26875 0,-0.5375c0,-0.13437 0,-0.26875 0,-0.26875c0,-0.13437 0,-0.26875 0,-0.40312c0,-0.13438 0,-0.26875 0,-0.40313c0,-0.13437 -0.13438,-0.26875 -0.13438,-0.40313c0,-0.13437 -0.13437,-0.26875 -0.13437,-0.40313c0,-0.13437 -0.13437,-0.26875 -0.13437,-0.40313c0,-0.13437 -0.13437,-0.26875 -0.13437,-0.26875c-0.13437,-0.13437 -0.13437,-0.26875 -0.26875,-0.26875c-0.13437,-0.13437 -0.13437,-0.13437 -0.26875,-0.26875c-0.13437,-0.13437 -0.13437,-0.13437 -0.26875,-0.26875c-0.13438,0 -0.26875,-0.13438 -0.40313,-0.13438c0,0 0,0 -0.13437,0c-0.13437,0 -0.13437,-0.13437 -0.26875,-0.13437c-0.13437,0 -0.26875,-0.13438 -0.40312,-0.13438c-0.13437,0 -0.26875,-0.13437 -0.40313,-0.13437c-0.13437,0 -0.13437,0 -0.26875,-0.13438c0,0 0,0 -0.13437,0c-0.13437,0 -0.26875,0 -0.40313,0c-0.13437,0 -0.26875,0 -0.40312,0c-0.13437,0 -0.26875,0 -0.40313,0c-0.13437,0 -0.26875,0 -0.40312,0c-0.13437,0 -0.26875,0.13438 -0.40313,0.13438c-0.13437,0 -0.26875,0 -0.26875,0.13437c-0.13437,0 -0.26875,0.13438 -0.40313,0.26875c-0.13437,0 -0.13437,0.13438 -0.26875,0.13438c-0.13437,0.13438 -0.26875,0.13438 -0.40313,0.26875c-0.13437,0.13438 -0.13437,0.13438 -0.26875,0.26875c-0.13437,0.13438 -0.13437,0.26875 -0.26875,0.26875c-0.13437,0.13438 -0.13437,0.26875 -0.26875,0.26875c0,0 0,0 0,0.13438l-50.65938,83.71563c-1.20938,1.88125 -0.5375,4.43438 1.34375,5.50938c0.67188,0.40312 1.34375,0.5375 2.15,0.5375c1.34375,0 2.6875,-0.67187 3.49375,-1.88125l38.83437,-64.09688l-8.86875,44.6125c-0.26875,1.20937 0.13438,2.41875 0.80625,3.35938c0.80625,0.94062 1.88125,1.47812 3.09063,1.47812h18.94687l-35.34063,48.50938l8.46563,-33.72812c0.40313,-2.28438 -0.80625,-4.43438 -3.09062,-4.97188z"></path></g></g></svg>
                    </div>
                    </br>
                    </br>
                       <h3>Étape 2 : Vous consommez votre propre électricité </h3>
                        
                    </div>
                    </center>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="single_transport">
                    <center>
                        <div class="icon">
                        
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="64" height="64" viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#009900"><path d="M28.0849,55.09375c-3.49375,0 -6.71875,1.61355 -8.73437,4.43542c-2.01562,2.82188 -2.5526,6.44895 -1.4776,9.67395c0.5375,1.74688 2.14843,2.82135 3.76093,2.82135c0.40313,0 0.80678,0.00105 1.2099,-0.2677c2.15,-0.67187 3.22553,-2.9573 2.55365,-5.1073c-0.26875,-0.80625 -0.13595,-1.7453 0.40155,-2.41718c0.5375,-0.67187 1.34585,-1.07605 2.1521,-1.07605h101.98853c4.03125,0 7.66147,2.55417 8.87085,6.45105l21.09583,62.61823c0.40312,1.075 0.00105,1.8802 -0.2677,2.28333c-0.40312,0.67188 -1.07552,1.07552 -1.88177,1.2099c-2.15,0.40312 -3.7625,2.41927 -3.35937,4.56927c0.26875,2.01563 2.01563,3.35938 4.03125,3.35938h0.67188c3.09062,-0.5375 5.91198,-2.28595 7.52448,-4.97345c1.47813,-2.82188 1.88073,-6.04583 0.80573,-9.00208l-20.82812,-62.61822c-2.41875,-7.12187 -9.00155,-11.9599 -16.52655,-11.9599zM14.6474,81.96875c-3.49375,0 -6.71875,1.61355 -8.73438,4.43542c-2.01562,2.82187 -2.5526,6.44895 -1.4776,9.67395l10.07812,30.36822l-10.4823,39.1026c-0.5375,2.15 0.67135,4.30157 2.82135,4.97345c0.5375,0.13438 0.80677,0.13385 1.2099,0.13385c1.74687,0 3.3599,-1.20833 3.8974,-2.9552l10.75,-40.3125v-0.40417v-0.40417v-0.5354v-0.27032c0,-0.13437 -0.13385,-0.40365 -0.13385,-0.53802v-0.13385l-10.4823,-31.57812c-0.26875,-0.80625 -0.13333,-1.7453 0.40417,-2.41718c0.5375,-0.67187 1.34323,-1.07605 2.14948,-1.07605h101.99115c4.03125,0 7.65885,2.55417 8.86823,6.45105l8.60052,25.79895h-31.7146l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.22552 -5.10468,-2.55365c-2.15,0.67188 -3.22553,2.9573 -2.55365,5.1073l7.12292,21.5h-31.7146l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.22552 -5.10468,-2.55365c-2.15,0.67188 -3.22553,2.9573 -2.55365,5.1073l7.12292,21.5h-20.29272c-2.28438,0 -4.03125,1.74688 -4.03125,4.03125c0,2.28438 1.74687,4.03125 4.03125,4.03125h25.93542h0.13385h0.13385h40.17865h0.13385h0.13647h37.35468l9.5401,28.75677c0.26875,0.80625 0.13595,1.7453 -0.40155,2.41718c-0.5375,0.67188 -1.34323,1.07605 -2.14948,1.07605h-28.35522l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.22552 -5.10468,-2.55365c-2.15,0.67188 -3.22553,2.9573 -2.55365,5.1073l7.25678,21.63385h-31.84845l-8.0625,-24.05365c-0.67187,-2.15 -2.95468,-3.2229 -5.10468,-2.55103c-2.15,0.67188 -3.22553,2.95468 -2.55365,5.10468l7.25678,21.63385h-25.1297c-4.03125,0 -7.65885,-2.55155 -8.86823,-6.44843l-1.4776,-4.30157c-0.67187,-2.15 -2.9573,-3.2229 -5.1073,-2.55103c-2.15,0.67188 -3.2229,2.95468 -2.55103,5.10468l1.4776,4.30157c2.41875,7.12187 9.00155,11.95728 16.52655,11.95728h30.63855h40.3125h31.0401c3.49375,0 6.71875,-1.61093 8.73438,-4.4328c2.01563,-2.82188 2.5526,-6.45157 1.4776,-9.67657l-20.96198,-62.88593c-2.41875,-7.12188 -9.00155,-11.9599 -16.52655,-11.9599z"></path></g></g></svg>
                        </div>
                        </br>
                        </br>
                        <h3>Étape 3 : Votre surplus d’électricité est stocké ou vendu à EDF</h3>
                    </div>
                    </center>
                </div>
            </div>

            <div class="row d-flex align-items-center my-5">
					<div class="col-md-4">
						<div class="embed-responsive embed-responsive-1by1">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/y-xW2nG9oVg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
						</div>

						<p class="shadow p-2 text-center">
							L'autoconsomation solaire selon Jamy
						</p>
					</div>

					<div class="col-md-8">
                    <div class="alert alert-success" role="alert">
                    <p>
							Les panneaux photovoltaïques récupèrent la lumière du soleil pour produire de l’électricité. L’avantage d’un tel processus est qu'il permet de disposer d’une source d’énergie inépuisable. En effet, le soleil produit assez de rayonnement pour alimenter 10 000 fois la consommation énergétique terrestre.
						</p>
 						<br>
 						<p>
							Les panneaux photovoltaïques permettent donc de canaliser cette énergie et de la récupérer pour vous. La qualité des derniers modèles de panneaux photovoltaïques est un réel avantage, car ceux-ci sont conçus pour une durée de vie de 20 à 30 ans. Ce qui vous permet de rentabiliser votre investissement.
						</p>
</div>
						
					</div>
				</div>
            
                <div class="row" style="padding-top:0px;">

                <div class="col-xl-4 col-lg-4 col-md-6">
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <a href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateurform" class="boxed-btn3" style="background-color:#090;">Tester mon éligibilité</a>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                </div>
            </div>
            
            <div class="row d-flex align-items-center" style="padding-top:100px;">
					<div class="col-md-3">
						<div class="trait bg-success w-100">
							&nbsp;
						</div>
					</div>

					<div class="col-md-6">
						<h2><center>Comment ça marche ?</center></h2>
                        <br>
					</div>

					<div class="col-md-3">
						<div class="trait bg-success w-100">
							&nbsp;
						</div>
					</div>
            </div>
            
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="row d-flex align-items-center">
                        <div class="col-md-6">
                            <a href="#form" class="js-scrollTo">
                                <img class="img-fluid" src="./img/france.png" alt="Carte de France">
                            </a>
                        </div>
                

                        <div class="col-md-6">
                            <div class="row mt-3">
                                <div class="col-12">
                                    <div class="col-12">
                                        <div class="row d-flex align-items-center">
                                            <div class="col-md-2 text-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            width="64" height="64"
                                            viewBox="0 0 172 172"
                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#009900"><path d="M32.25,37.625c-9.675,0 -17.46875,7.79375 -17.46875,17.46875v79.28125c0,2.28438 1.74687,4.03125 4.03125,4.03125c2.28438,0 4.03125,-1.74687 4.03125,-4.03125v-79.28125c0,-5.24063 4.16563,-9.40625 9.40625,-9.40625h107.5c5.24062,0 9.40625,4.16562 9.40625,9.40625v79.28125c0,2.28438 1.74687,4.03125 4.03125,4.03125c2.28438,0 4.03125,-1.74687 4.03125,-4.03125v-79.28125c0,-9.675 -7.79375,-17.46875 -17.46875,-17.46875zM5.375,145.125c-2.28438,0 -4.03125,1.74687 -4.03125,4.03125v4.03125c0,9.675 7.79375,17.46875 17.46875,17.46875h134.375c9.675,0 17.46875,-7.79375 17.46875,-17.46875v-4.03125c0,-2.28438 -1.74687,-4.03125 -4.03125,-4.03125h-47.03125c-2.28437,0 -4.03125,1.74687 -4.03125,4.03125c0,2.28438 1.74688,4.03125 4.03125,4.03125h43c0,5.24062 -4.16563,9.40625 -9.40625,9.40625h-134.375c-5.24062,0 -9.40625,-4.16563 -9.40625,-9.40625h43c2.28438,0 4.03125,-1.74687 4.03125,-4.03125c0,-2.28438 -1.74687,-4.03125 -4.03125,-4.03125zM72.5625,145.125c-2.28437,0 -4.03125,1.74687 -4.03125,4.03125c0,2.28438 1.74688,4.03125 4.03125,4.03125h26.875c2.28437,0 4.03125,-1.74687 4.03125,-4.03125c0,-2.28438 -1.74688,-4.03125 -4.03125,-4.03125z"></path></g></g></svg>
                                                                                        </div>
                                                                                        <div class="col-md-10">
                                                                                            <p class="mb-1">Une simulation en ligne rapide et gratuite</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mt-3">
                                                                            <div class="col-12">
                                                                                <div class="col-12 d-flex align-items-center">
                                                                                    <div class="row">
                                                                                        <div class="col-md-2 text-center">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            width="64" height="64"
                                            viewBox="0 0 172 172"
                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#009900"><path d="M24.72553,59.66303c-2.28438,0 -4.03125,1.74688 -4.03125,4.03125v80.625c0,2.28438 1.74687,4.03125 4.03125,4.03125h10.21197c0,0.5375 -0.13385,0.9401 -0.13385,1.4776c0,3.225 0.94115,6.3151 2.55365,8.86823h-13.1698c-2.28438,0 -4.03125,1.74687 -4.03125,4.03125c0,2.28438 1.74687,4.03125 4.03125,4.03125h113.41302c0.40312,0 0.94063,-0.13333 1.34375,-0.2677c8.6,-0.67187 15.45313,-7.92865 15.45313,-16.66302c0,-0.5375 0.00052,-0.9401 -0.13385,-1.4776h10.34583c2.28438,0 4.03125,-1.74687 4.03125,-4.03125v-47.03125v-0.27032v-0.5354c0,-0.13437 0.00052,-0.13595 -0.13385,-0.27032c0,-0.13437 -0.13385,-0.26718 -0.13385,-0.40155l-8.46667,-21.09845c-3.62813,-9.1375 -12.3625,-14.91457 -22.17187,-15.04895h-13.4375c-2.28437,0 -4.03125,1.74688 -4.03125,4.03125v76.59375h-54.82605c-1.075,-1.6125 -2.41665,-2.95625 -3.89478,-4.03125h50.79218c2.28438,0 4.03125,-1.74687 4.03125,-4.03125v-68.53125c0,-2.28437 -1.74687,-4.03125 -4.03125,-4.03125zM28.75678,67.59167h79.4151v47.03125h-79.4151zM128.46197,67.59167h5.375v22.84375c0,1.88125 1.21042,3.49165 3.09167,3.89478l23.78333,5.91302v39.91095h-9.13593c-3.09062,-4.43437 -8.06355,-7.25677 -13.84167,-7.25677c-3.35937,0 -6.5849,1.07447 -9.2724,2.82135zM141.89948,68.12708c4.8375,1.20937 8.7349,4.70313 10.61615,9.40625l5.50885,13.84167l-16.125,-4.03125zM28.89063,136.12292h12.89947c-1.47812,1.075 -2.8224,2.41875 -3.8974,4.03125h-9.00208zM51.60053,140.68958c3.225,0 6.17968,1.74792 7.79218,4.43542c0.13437,0.67188 0.40522,1.20833 0.80835,1.61145c0.26875,0.94063 0.5354,2.0172 0.5354,2.95782c0,4.43437 -3.225,8.06198 -7.39062,8.86823h-3.35937c-4.16563,-0.80625 -7.39062,-4.43385 -7.39062,-8.86823c0,-4.97188 4.03282,-9.0047 9.0047,-9.0047zM137.46667,140.68958c4.97188,0 9.00208,4.03282 9.00208,9.0047c0.26875,4.70313 -3.3599,8.46458 -7.92865,9.00208c-0.26875,-0.13438 -0.53645,-0.13385 -0.93958,-0.13385h-1.74792c-4.16563,-0.80625 -7.39063,-4.43385 -7.39063,-8.86823c0,-4.97188 4.03282,-9.0047 9.0047,-9.0047zM68.26355,148.21667h52.67395c0,0.5375 -0.13385,0.9401 -0.13385,1.4776c0,3.225 0.94115,6.3151 2.55365,8.86823h-57.51355c1.6125,-2.55312 2.55365,-5.64323 2.55365,-8.86823c0,-0.5375 0.00052,-0.9401 -0.13385,-1.4776zM6.71875,158.66223c-1.04141,0 -2.08229,0.3711 -2.82135,1.11017c-0.80625,0.67188 -1.2099,1.74635 -1.2099,2.82135c0,1.075 0.40365,2.14948 1.2099,2.82135c0.80625,0.80625 1.74635,1.2099 2.82135,1.2099c1.075,0 2.14948,-0.40365 2.82135,-1.2099c0.80625,-0.80625 1.2099,-1.74635 1.2099,-2.82135c0,-1.075 -0.40365,-2.14948 -1.2099,-2.82135c-0.73906,-0.73906 -1.77994,-1.11017 -2.82135,-1.11017z"></path></g></g></svg>
                                                                                        </div>
                                                                                        <div class="col-md-10">
                                                                                            <p class="mb-1">Une visite technique offerte par un expert RGE de votre secteur</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mt-3">
                                                                            <div class="col-12">
                                                                                <div class="col-12">
                                                                                    <div class="row d-flex align-items-center">
                                                                                        <div class="col-md-2 text-center">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            width="64" height="64"
                                            viewBox="0 0 172 172"
                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#009900"><path d="M45.01563,12.09375c-9.675,0 -17.46875,7.79375 -17.46875,17.46875v107.5c0,9.675 7.79375,17.46875 17.46875,17.46875h81.96875c9.675,0 17.46875,-7.79375 17.46875,-17.46875v-107.5c0,-9.675 -7.79375,-17.46875 -17.46875,-17.46875zM45.01563,20.15625h81.96875c5.24062,0 9.40625,4.16563 9.40625,9.40625v107.5c0,5.24062 -4.16563,9.40625 -9.40625,9.40625h-81.96875c-5.24063,0 -9.40625,-4.16563 -9.40625,-9.40625v-107.5c0,-5.24062 4.16562,-9.40625 9.40625,-9.40625zM45.01563,29.5625v20.15625h81.96875v-20.15625zM50.39063,68.53125c-2.28438,0 -4.03125,1.74688 -4.03125,4.03125c0,2.28437 1.74687,4.03125 4.03125,4.03125h68.53125c2.28437,0 4.03125,-1.74688 4.03125,-4.03125c0,-2.28437 -1.74688,-4.03125 -4.03125,-4.03125zM50.39063,88.6875c-2.28438,0 -4.03125,1.74688 -4.03125,4.03125c0,2.28437 1.74687,4.03125 4.03125,4.03125h68.53125c2.28437,0 4.03125,-1.74688 4.03125,-4.03125c0,-2.28437 -1.74688,-4.03125 -4.03125,-4.03125zM50.39063,108.84375c-2.28438,0 -4.03125,1.74688 -4.03125,4.03125c0,2.28437 1.74687,4.03125 4.03125,4.03125h35.60938c2.28437,0 4.03125,-1.74688 4.03125,-4.03125c0,-2.28437 -1.74688,-4.03125 -4.03125,-4.03125z"></path></g></g></svg>
                                                                                        </div>
                                                                                        <div class="col-md-10">
                                                                                            <p class="mb-1">Une visite technique offerte par un expert RGE de votre secteur</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mt-3">
                                                                            <div class="col-12">
                                                                                <div class="col-12">
                                                                                    <div class="row d-flex align-items-center">
                                                                                        <div class="col-md-2 text-center">
                                                                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            width="64" height="64"
                                            viewBox="0 0 172 172"
                                            style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#009900"><path d="M20.15625,147.54375l64.5,22.84375v0c0.13438,0 0.26875,0.13438 0.40313,0.13438c0.26875,0.13437 0.67188,0.13437 0.94062,0.13437c0.26875,0 0.40313,0 0.67188,0v0c0,0 0,0 0.13437,0c0.13437,0 0.40313,-0.13437 0.5375,-0.13437v0l64.5,-22.84375c1.6125,-0.5375 2.6875,-2.15 2.6875,-3.7625v-45.28437l13.4375,-4.70312c1.075,-0.40313 2.01563,-1.34375 2.41875,-2.41875c0.40313,-1.075 0.26875,-2.41875 -0.40312,-3.49375l-16.125,-26.875l-0.13438,-0.13438c0,-0.13437 -0.13437,-0.13437 -0.13437,-0.26875v0c0,-0.13437 -0.13438,-0.13437 -0.13438,-0.26875c0,0 0,0 0,-0.13437c-0.13437,-0.13438 -0.13437,-0.13438 -0.26875,-0.26875l-0.13438,-0.13438l-0.13437,-0.13437h-0.13438l-0.13437,-0.13437c-0.13438,-0.13438 -0.26875,-0.13438 -0.40313,-0.13438c-0.13437,0 -0.13437,-0.13437 -0.26875,-0.13437v0v0l-64.5,-22.84375c0,0 0,0 -0.13437,0h-0.13437h-0.13437c-0.13437,0 -0.13437,0 -0.26875,0v0v0c-0.13437,0 -0.13437,0 -0.26875,0c-0.13437,0 -0.13437,0 -0.26875,0c-0.13438,0 -0.26875,0 -0.5375,0c-0.13437,0 -0.13437,0 -0.26875,0c-0.26875,0 -0.5375,0.13437 -0.67187,0.13437l-64.5,22.84375c-0.26875,0.13438 -0.40312,0.13438 -0.67187,0.26875l-0.13437,0.13437c-0.13437,0.13438 -0.26875,0.13438 -0.40313,0.26875l-0.13437,0.13438c-0.13437,0.13437 -0.26875,0.13437 -0.26875,0.26875l-0.13438,0.13437c-0.13437,0.13438 -0.13437,0.26875 -0.26875,0.26875c0,0 0,0.13438 -0.13438,0.13438l-16.125,26.875c-0.94062,1.47812 -0.80625,3.35938 0.26875,4.56875c0.80625,0.94062 1.88125,1.47812 3.09062,1.47812c0.40313,0 0.94062,-0.13437 1.34375,-0.26875l10.75,-3.7625v53.75c0,1.74687 1.075,3.225 2.6875,3.7625zM160.57813,87.88125l-56.70625,20.15625l-11.95938,-19.8875l56.70625,-20.15625zM90.03125,45.95625l48.375,17.2l-48.375,17.2zM90.03125,100.5125l8.6,14.37812c0.80625,1.34375 2.15,2.01563 3.49375,2.01563c0.40313,0 0.94062,-0.13437 1.34375,-0.26875l43,-15.18438v39.50625l-56.4375,20.02188zM25.53125,68.8l56.4375,20.02188v72.025l-56.4375,-20.02187z"></path></g></g></svg>
                                            </div>
                                            <div class="col-md-10">
                                                <p class="mb-1">L'Installation de vos panneaux sans avance d'argent</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-5">
			<div class="col-md-10 offset-md-1">
				<p>
					Les panneaux photovoltaïques sont également très efficaces dans les régions où le ciel est gris car la lumière du soleil passe à travers les nuages. N'hésitez plus et testez votre éligibilité ici.
				</p>

				<a href="#form" class="btn btn-success btn-lg w-100 big js-scrollTo">Estimer mes économies</a>
			</div>
		</div>
                            </div>
                        </div>
                    </div>
                </div>
		    </div>
        
        </div>
    </div>

   


    <!-- contact_action_area  -->
    <div class="contact_action_area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-7 col-md-6">
                    <div class="action_heading">
                        <h3>Plan solaire 2020 : Ne payez plus vos factures d'électricité</h3>
                        <p>Nouveau programme zéro dépense !</p>
                    </div>
                </div>
                <div class="col-xl-5 col-md-6">
                    <div class="call_add_action">
                        <a href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateurform" class="boxed-btn3" style="background-color:#090;">Estimer mes économies</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
			<div class="col-12" style="background-color:#090">
				<div class="container">
					<div class="row mt-2 mt-md-5">
						<div class="col-md-5 offset-md-1">
							<h1 class="text-white h2 text-center text-md-left title">
								Vous voulez en finir avec les factures d'électricité
							</h1>

							<p class="text-white lead text-center text-md-left">Optez pour l'autoconsommation</p>

							<div class="row">
								<div class="col-12 mt-0 mt-md-2 text-warning">
									<div class="row">
										<div class="col-12">
											<ul>
												<li>
													<span class="text-white">Installation financée grâce au plan d’Etat 2020</span>
												</li>
												<li>
													<span class="text-white">9 milliards d’euros débloqués par le gouvernement</span></li>
												
												<li>
													<span class="text-white">Plus-value immobilière de 15 à 25 %</span>
												</li>
												<li>
													<span class="text-white">Matériel et production garantis 25 ans</span>
												</li>
												<li>
													<span class="text-white">Installation réalisée par un technicien agréé RGE</span>
												</li>
												<li>
													<span class="text-white">Aucune démarche administrative</span>
												</li>
												<li>
													<span class="text-white">Revenu complémentaire de 1500 à 3500€ par an</span>
												</li>
                                            </ul>
                                        </br>
                                        </br>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div id="form" class="col-md-5">
							<div class="row d-none d-md-block">
								<div class="col-md-10 offset-md-1 text-center mb-2">
                            </br>
                            </br>
									<a href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateurform" class="btn btn-warning font-weight-bold">
										Demande gratuite en 30 secondes Données personnelles sécurisées
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <section class="ftco-counter bg-light ftco-no-pt" id="simulateur">
    	<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <br>
                    <p>Comment les panneaux photovoltaïques sont-ils devenus aussi rentables ?</p>
                    <p>Elisabeth Borne ministre de la Transition écologique et solidaire Lance le Plan solaire 2020. l’État mise sur l’énergie solaire et débloque plus de 9 milliards d’euros pour équiper les foyers français afin d'accéder à l’autoconsommation d’électricité.</p>
                    <p>Après la crise sanitaire, l’heure est au changement pour la France qui a pour objectif d’assurer aux propriétaires éligibles d’avoir <span class="font-weight-bold">un projet rentable clé en main</span> sans aucune démarche et sans toucher à leur pouvoir d’achat.</p>
                    <p>
					L’Etat débloque des aides pour votre projet et vous donne droit à des subventions colossales : ne payez plus vos factures d’électricité et <span class="font-weight-bold">gagnez de l’argent</span> grâce à votre installation solaire !
                    </p>
                    <p>
					De plus, avec l'évolution des technologies, les nouveaux panneaux solaires Aérovoltaïque ont vu le jour : en plus de produire votre électricité vous pourrez également <span class="font-weight-bold">produire votre chauffage et climatisation.</span> <br>
					Un panneau 3 en 1 qui permet d’avoir une <span class="font-weight-bold">auto consommation totale</span> ! 
				    </p>
                    <p>
					Notre <span class="font-weight-bold">réseau d’experts RGE</span> partenaire du programme vous aideront à trouver l’installation qui correspond à vos besoins et vous permettront d’assurer votre autonomie avec une prise en charge complète de votre projet.
				    </p>
                    <p>
					N’attendez plus : devenez autonome et réduisez votre facture d’énergie sans changer votre mode de consommation !
				    </p>
                    <p>
					Plus de 2.000.000 foyers ont déjà bénéficié d’une installation solaire et réalisent <span class="font-weight-bold">des économies considérables</span>, pourquoi pas vous ? 
				    </p>
                    </div>
                    </br>
            </div>
        </div>
    </section>

    <div class="transportaion_area" id="simulateurform">
        <div class="container">
        <div class="row d-flex align-items-center">
					<div class="col-md-3">
						<div class="trait bg-success w-100">
							&nbsp;
						</div>
					</div>

					<div class="col-md-6">
						<h2><center>Simulateur</center></h2>
                        <br>
					</div>

					<div class="col-md-3">
						<div class="trait bg-success w-100">
							&nbsp;
						</div>
					</div>
				</div>
            <div class="row">
                <div style="width:100%; text-align:center;">
                    <i class="fa fa-lock" style="color:#390"></i>&nbsp;
                    <span style="color:#390;">Demande gratuite en 30 secondes Données personnelles sécurisées <br /><br /></span>        
                </div>
            </div>
            <form method="post" action="<?php echo $_SERVER['SCRIPT_URL']; ?>">
            <input type="hidden" name="submit_lead" value="1" />
            <input type="hidden" name="type_lead" value="pompe" />

            <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Vous êtes ?</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
                <option selected>Sélectionner...</option>
                <option value="Propriétaire">Propriétaire</option>
                <option value="Locataire">Locataire</option>
            </select>
            </div>
            
            <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Vous habitez ?</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
                <option selected>Sélectionner...</option>
                <option value="maison">Maison</option>
                <option value="Appartement">Appartement</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Votre département</label>
            </div>
            <select class="custom-select" id="inputGroupSelect01">
                <option selected>Sélectionner...</option>
				<option value="1">01 - Ain </option>
				<option value="2">02 - Aisne </option>
				<option value="3">03 - Allier</option>
				<option value="4">04 - Alpes de Haute Provence</option>
				<option value="5">05 - Hautes Alpes</option>
				<option value="6">06 - Alpes Maritimes</option>
				<option value="7">07 - Ardèche</option>
				<option value="8">08 - Ardennes</option>
				<option value="9">09 - Ariège</option>
				<option value="10">10 - Aube</option>
				<option value="11">11 - Aude</option>
				<option value="12">12 - Aveyron</option>
				<option value="13">13 - Bouches du Rhône</option>
				<option value="14">14 - Calvados</option>
				<option value="15">15 - Cantal</option>
				<option value="16">16 - Charente</option>
				<option value="17">17 - Charente Maritime</option>
				<option value="18">18 - Cher</option>
				<option value="19">19 - Corrèze</option>
				<option value="2A">2A - Corse du Sud</option>
				<option value="2B">2B - Haute-Corse</option>
				<option value="21">21 - Côte d'Or</option>
				<option value="22">22 - Côtes d'Armor</option>
				<option value="23">23 - Creuse</option>
			    <option value="24">24 - Dordogne</option>
				<option value="25">25 - Doubs</option>
				<option value="26">26 - Drôme</option>
				<option value="27">27 - Eure</option>
				<option value="28">28 - Eure et Loir</option>
				<option value="29">29 - Finistère</option>
				<option value="30">30 - Gard</option>
				<option value="31">31 - Haute Garonne</option>
				<option value="32">32 - Gers</option>
				<option value="33">33 - Gironde</option>
				<option value="34">34 - Hérault</option>
				<option value="35">35 - Ille et Vilaine</option>
				<option value="36">36 - Indre</option>
				<option value="37">37 - Indre et Loire</option>
				<option value="38">38 - Isère</option>
				<option value="39">39 - Jura</option>
				<option value="40">40 - Landes</option>
				<option value="41">41 - Loir et Cher</option>
				<option value="42">42 - Loire</option>
				<option value="43">43 - Haute Loire</option>
				<option value="44">44 - Loire Atlantique</option>
				<option value="45">45 - Loiret</option>
				<option value="46">46 - Lot</option>
				<option value="47">47 - Lot et Garonne</option>
				<option value="48">48 - Lozère</option>
				<option value="49">49 - Maine et Loire</option>
				<option value="50">50 - Manche</option>
				<option value="51">51 - Marne</option>
				<option value="52">52 - Haute Marne</option>
				<option value="53">53 - Mayenne</option>
				<option value="54">54 - Meurthe et Moselle</option>
				<option value="55">55 - Meuse</option>
				<option value="56">56 - Morbihan</option>
				<option value="57">57 - Moselle</option>
				<option value="58">58 - Nièvre</option>
				<option value="59">59 - Nord</option>
				<option value="60">60 - Oise</option>
				<option value="61">61 - Orne</option>
				<option value="62">62 - Pas de Calais</option>
				<option value="63">63 - Puy de Dôme</option>
				<option value="64">64 - Pyrénées Atlantiques</option>
				<option value="65">65 - Hautes Pyrénées</option>
				<option value="66">66 - Pyrénées Orientales</option>
				<option value="67">67 - Bas Rhin</option>
				<option value="68">68 - Haut Rhin</option>
				<option value="69">69 - Rhône</option>
				<option value="70">70 - Haute Saône</option>
				<option value="71">71 - Saône et Loire</option>
				<option value="72">72 - Sarthe</option>
				<option value="73">73 - Savoie</option>
				<option value="74">74 - Haute Savoie</option>
				<option value="75">75 - Paris</option>
				<option value="76">76 - Seine Maritime</option>
				<option value="77">77 - Seine et Marne</option>
				<option value="78">78 - Yvelines</option>
				<option value="79">79 - Deux Sèvres</option>
				<option value="80">80 - Somme</option>
				<option value="81">81 - Tarn</option>
				<option value="82">82 - Tarn et Garonne</option>
				<option value="83">83 - Var</option>
				<option value="84">84 - Vaucluse</option>
				<option value="85">85 - Vendée</option>
				<option value="86">86 - Vienne</option>
				<option value="87">87 - Haute Vienne</option>
				<option value="88">88 - Vosges</option>
				<option value="89">89 - Yonne</option>
				<option value="90">90 - Territoire de Belfort</option>
				<option value="91">91 - Essonne</option>
				<option value="92">92 - Hauts de Seine</option>
				<option value="93">93 - Seine Saint Denis</option>
				<option value="94">94 - Val de Marne</option>
				<option value="95">95 - Val d'Oise</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Nom & Prénom</span>
            </div>
            <input type="text" class="form-control" name="lead[nom]" placeholder="Nom & Prénom" required />  
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Email</span>
            </div>
            <input type="email" class="form-control" name="lead[email]" placeholder="Email" required /> 
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Téléphone</span>
            </div>
            <input type="number" class="form-control" name="lead[telephone]" placeholder="Numéro de téléphone" required /> 
        </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" style="width:100%;">Envoyer</button>
      </div>
      </form>
    </div>
            
            
            
            
        
        </div>
    </div>

    <!-- contact_location  -->
    <div class="contact_location">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="location_left">
                        <div class="logo">
                            <a href="simulateursimulateur">
                            <p style="color:white"> Logo Client </p>
                            </a>
                        </div>
                        <ul>
                            <li><a href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateur


"> <i class="fa fa-facebook"></i> </a></li>
                            <li><a href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateur


"> <i class="fa fa-google-plus"></i> </a></li>
                            <li><a href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateur


"> <i class="fa fa-twitter"></i> </a></li>
                            <li><a href="<?php echo $_SERVER['SCRIPT_URL']; ?>#simulateur


"> <i class="fa fa-youtube"></i> </a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-md-3">
                    <div class="single_location">
                        <h3><p style="color:white">  Phone </p></h3>
                        <p><p style="color:white">(xx)xxxx-xxxxx</p><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--/ contact_location  -->

    <!-- footer start -->
    <footer class="footer">
<?php
/*
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Services
                            </h3>
                            <ul>
                                <li><a href="#">Air Transportation</a></li>
                                <li><a href="#">Ocean Freight</a></li>
                                <li><a href="#">Ocean Cargo</a></li>
                                <li><a href="#">Logistics</a></li>
                                <li><a href="#">Warehouse Moving</a></li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Company
                            </h3>
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">News</a></li>
                                <li><a href="#"> Testimonials</a></li>
                                <li><a href="#"> Why Us?</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Industries
                            </h3>
                            <ul>
                                <li><a href="#">Chemicals</a></li>
                                <li><a href="#">Automotive</a></li>
                                <li><a href="#"> Consumer Goods</a></li>
                                <li><a href="#">Life Science</a></li>
                                <li><a href="#">Foreign Trade</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Subscribe
                            </h3>
                            <form action="#" class="newsletter_form">
                                <input type="text" placeholder="Enter your mail">
                                <button type="submit">Subscribe</button>
                            </form>
                            <p class="newsletter_text">Esteem spirit temper too say adieus who direct esteem esteems
                                luckily.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		*/
 ?> 
 <div class="container">
 <div class="row">
 <div class="col-ml-12 text-center" style="font-size:12px;text-align:center;">
</div>
</div>
</div>

        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved</i>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->
<!-- Button trigger modal -->
 <?php
}
?>
  <!-- Modal -->

    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <!-- <script src="js/gijgo.min.js"></script> -->
    <script src="js/slick.min.js"></script>



    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>


    <script src="js/main.js"></script>
    
    <script type="text/javascript">
        $('select').selectpicker();
    </script>



</body>

</html>